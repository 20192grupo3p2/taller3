bin/utilfecha: obj/main.o obj/dias.o obj/segundos.o obj/fecha.o
	gcc $^ -o bin/utilfecha
obj/main.o: src/main.c
	gcc -Wall -I include/ -c $^ -o obj/main.o
obj/dias.o: src/dias.c
	gcc -Wall -I include/ -c $^ -o obj/dias.o
obj/segundos.o: src/segundos.c
	gcc -Wall -I include/ -c $^ -o obj/segundos.o
obj/fecha.o: src/fecha.c
	gcc -Wall -I include/ -c $^ -o obj/fecha.o

.PHONY: clean
clean:
	rm bin/* obj/*

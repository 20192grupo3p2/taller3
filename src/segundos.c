#include <stdio.h>
#include "../include/utilfecha.h"

void segundos(int segundos){
	int horas=0;
	int minutos=0;
	int tempMin=0;
	int seconds=0;

	horas=segundos/HORA_SEGUNDOS;
	tempMin=segundos%HORA_SEGUNDOS;
	minutos=tempMin/MINUTOS_HORA;
	seconds=tempMin%SEGUNDOS_MINUTO;

	printf("horas\tminutos\tsegundos\n");
	printf("%d\t%d\t%d\n",horas,minutos,seconds);

}

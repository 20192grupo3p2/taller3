#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "../include/utilfecha.h"

int hflag=0;
int aflag=0;
int fflag=0;

int main (int argc, char **argv){
	int opt=0;
	int hora=0;
	int anio=0; 
	char *date;

	while((opt=getopt(argc,argv,"h:a:f:"))!=-1){
		switch(opt){
			case 'h':
				hora=atoi(optarg);
				hflag=1;
				break;
			case 'a':
				anio=atoi(optarg);
				aflag=1;
				break;
			case 'f':
				date=optarg;
				fflag=1;
				break;
			case '?':
				default:
					printf("Argumento no valido.\n");
					printf("Uso:\n %s [-argumento] <valor>\n", argv[0]);
					printf("Opciones:\n");
					printf(" -h\t\t\tConviete segundos ingresados a horas, minutos y segundos.\n");
					printf(" -a\t\t\tConviete dias ingresados a años, meses y dias.\n");
					printf(" -f \t\t\tLleva a otro formato la fecha ingresada.\n");
					return -1;
		}
	}

	if(argc>optind){
		printf("Valores exceden los argumentos dados.\n");
		printf("Uso:\n %s [-argumento] <valor>\n", argv[0]);
		printf("Opciones:\n");
		printf(" -h\t\t\tConviete segundos ingresados a horas, minutos y segundos.\n");
		printf(" -a\t\t\tConviete dias ingresados a años, meses y dias.\n");
		printf(" -f \t\t\tLleva a otro formato la fecha ingresada.\n");
		return -1;
	}

	if(hflag){
		segundos(hora);
	}
	if(aflag){
		dias(anio);
	}
	if(fflag){
		fecha(date);
	}
	return 0;
}



